package com.example.rmaprojekt;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class budgetCategoryActivity extends AppCompatActivity {
    private Button createButton, deleteButton;
    private String parentName , name;
    private TextView categoryName;
    private ListView mListView;
    DatabaseHelper mDatabaseHelper;
    private ArrayList<String> transactionIds;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget_sub_item);

        Intent intent = getIntent();

        if (intent.hasExtra("parentName")) {
            parentName = getIntent().getExtras().get("parentName").toString();
        } else {
            parentName = "";
        }
        if (intent.hasExtra("name")) {
            name = getIntent().getExtras().get("name").toString();
        } else {
            name = "";
        }
        mDatabaseHelper = new DatabaseHelper(this);
        categoryName = (TextView) findViewById(R.id.categoryName);
        mListView = (ListView) findViewById(R.id.transactionListView);
        createButton = (Button) findViewById(R.id.transactionCreateButton);
        deleteButton = (Button) findViewById(R.id.deleteCategory);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteCategory();
            }
        });
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCreateTransaction();
            }
        });
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String transactionId = transactionIds.get(i);
                openTransaction(transactionId);
            }
        });
        categoryName.setText(name);
        populateListView();
    }

    private void openTransaction(String transactionId) {
        Intent intent = new Intent(this, transactionActivity.class);
        intent.putExtra("transactionId", transactionId);
        intent.putExtra("parentName", parentName);
        intent.putExtra("name", name);
        startActivity(intent);
    }

    public void openCreateTransaction(){
        Intent intent = new Intent(this, createTransactionActivity.class);
        intent.putExtra("parentName", parentName);
        intent.putExtra("name", name);
        startActivity(intent);
    }
    public void deleteCategory(){
        mDatabaseHelper.deleteCategory(name,parentName);
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("currentBudget", parentName);
        startActivity(intent);
    }
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("currentBudget", parentName);
        startActivity(intent);
    }
    private void populateListView() {
        Cursor cursor = mDatabaseHelper.getTransactions(parentName, name);

        ArrayList<String> listData = new ArrayList<>();
        transactionIds = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                String transactionId = cursor.getString(cursor.getColumnIndex("ID"));
                String description = cursor.getString(cursor.getColumnIndex("description"));
                String amount = cursor.getString(cursor.getColumnIndex("transactionAmount"));
                transactionIds.add(transactionId);
                listData.add(description+": "+ amount+ " kn");
            } while (cursor.moveToNext());
        }
        ListAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listData);
        mListView.setAdapter(adapter);
    }
}
