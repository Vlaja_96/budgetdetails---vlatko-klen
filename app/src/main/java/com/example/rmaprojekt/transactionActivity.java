package com.example.rmaprojekt;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class transactionActivity extends AppCompatActivity {

    private String transactionId, parentName, name;
    private EditText transactionDescription, transactionAmount;
    private Button transactionSave, transactionDelete;
    DatabaseHelper mDatabaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);

        Intent intent = getIntent();
        if (intent.hasExtra("transactionId")) {
            transactionId = getIntent().getExtras().get("transactionId").toString();
        } else {
            transactionId = "";
        }
        if (intent.hasExtra("parentName")) {
            parentName = getIntent().getExtras().get("parentName").toString();
        } else {
            parentName = "";
        }
        if (intent.hasExtra("name")) {
            name = getIntent().getExtras().get("name").toString();
        } else {
            name = "";
        }

        transactionDescription = (EditText) findViewById(R.id.transactionDescription);
        transactionAmount = (EditText) findViewById(R.id.transactionAmount);
        transactionSave = (Button) findViewById(R.id.transactionSave);
        transactionDelete = (Button) findViewById(R.id.transactionDelete);
        mDatabaseHelper = new DatabaseHelper(this);
        transactionSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveTransaction();
            }
        });
        transactionDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteTransaction();
            }
        });
        populateTransaction();
    }

    public void saveTransaction() {
        mDatabaseHelper.updateTransaction(transactionDescription.getText().toString(), transactionAmount.getText().toString(), transactionId);
        Intent intent = new Intent(this, budgetCategoryActivity.class);
        intent.putExtra("parentName", parentName);
        intent.putExtra("name", name);
        startActivity(intent);
    }
    public void deleteTransaction() {
        mDatabaseHelper.deleteTransaction(transactionId);
        Intent intent = new Intent(this, budgetCategoryActivity.class);
        intent.putExtra("parentName", parentName);
        intent.putExtra("name", name);
        startActivity(intent);
    }

    private void populateTransaction() {
        Cursor cursor = mDatabaseHelper.getTransaction(transactionId);
        if (cursor.moveToFirst()) {
            do {
                transactionDescription.setText(cursor.getString(cursor.getColumnIndex("description")));
                transactionAmount.setText(cursor.getString(cursor.getColumnIndex("transactionAmount")));
            } while (cursor.moveToNext());
        }
    }
}
