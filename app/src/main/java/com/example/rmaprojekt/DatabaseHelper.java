package com.example.rmaprojekt;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHelper";

    private static final String TABLE_NAME = "budgets_table";
    private static final String COL0  = "ID";
    private static final String COL1 = "name";
    private static final String COL2 = "totalBudget";
    private static final String COL3 = "budgetDifference";
    private static final String COL4 = "categories";

    private static final String SUB_TABLE_NAME = "categories_table";
    private static final String SUBCOL0  = "ID";
    private static final String SUBCOL1 = "name";
    private static final String SUBCOL2 = "parentName";

    private static final String TRANSACTION_TABLE_NAME = "transaction_table";
    private static final String TRANSACTIONCOL0 = "ID";
    private static final String TRANSACTIONCOL1 = "parentName";
    private static final String TRANSACTIONCOL2 = "name";
    private static final String TRANSACTIONCOL3 = "transactionAmount";
    private static final String TRANSACTIONCOL4 = "description";


    public DatabaseHelper(Context context) {
        super(context, TABLE_NAME,null, 1);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE "+ TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COL1 +" TEXT,"+ COL2 +" TEXT," + COL3 +" TEXT,"+ COL4 +" TEXT)";
        db.execSQL(createTable);
        String createTable2 = "CREATE TABLE "+ SUB_TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                SUBCOL1 +" TEXT,"+ SUBCOL2 +" TEXT)";
        db.execSQL(createTable2);
        String createTable3 = "CREATE TABLE "+ TRANSACTION_TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TRANSACTIONCOL1 +" TEXT,"+TRANSACTIONCOL2 +" TEXT,"+TRANSACTIONCOL3 +" TEXT,"+ TRANSACTIONCOL4 +" TEXT)";
        db.execSQL(createTable3);
    }
  /*  public void recreatetables(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + SUB_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TRANSACTION_TABLE_NAME);
        onCreate(db);

    }*/
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
    public boolean addData(String item) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1, item);
        contentValues.put(COL2, "0");
        contentValues.put(COL3, "0");
        contentValues.put(COL4, "");
        Log.d(TAG, "addData: Adding " + item + " to "+ TABLE_NAME);
        long result = db.insert(TABLE_NAME, null, contentValues);
        if (result == -1) {
            return false;
        }
        else{
            return true;
        }
    }
    public Cursor getData(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor data = db.rawQuery(query,null);
        return data;
    }
    public Cursor getBudget(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COL1 + " = \"" + name+"\"";
        Cursor data = db.rawQuery(query,null);
        return data;
    }

    public boolean addCategory(String name, String parentName){
        String categories;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SUBCOL1, name);
        contentValues.put(SUBCOL2, parentName);
        Log.d(TAG, "addData: Adding " + name + " to "+ SUB_TABLE_NAME);
        long result = db.insert(SUB_TABLE_NAME, null, contentValues);
        if (result == -1) {
            return false;
        }
        else{
            Cursor cursor = getBudget(parentName);
            if (cursor.moveToFirst()) {
                do {
                    categories = cursor.getString(cursor.getColumnIndex("categories"));
                    categories+=(name+",");
                    updateBudgetCategories(categories,parentName);
                } while (cursor.moveToNext());
            }
            return true;
        }
    }

    public void updateBudgetCategories(String categories, String parentName){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_NAME +" SET "+ COL4 +" = \""+ categories +"\" WHERE " + COL1 + " = \"" + parentName+"\"";
        db.execSQL(query);
    }

    public void deleteCategory(String name, String parentName){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + SUB_TABLE_NAME +" WHERE "+ SUBCOL1 +" = \""+ name +"\" AND "+ SUBCOL2 +" = \""+ parentName +"\"";
        db.execSQL(query);

        Cursor cursor = getBudget(parentName);
        List<String> list = new ArrayList<String>();
        if (cursor.moveToFirst()) {
            do {
                String categories = cursor.getString(cursor.getColumnIndex("categories"));
                String[] items = categories.split(",");
                for(int i=0; i < items.length; i++){
                    list.add(items[i]);
                }
                list.remove(name);
                StringBuilder csvList = new StringBuilder();
                for(String s : list){
                    csvList.append(s);
                    csvList.append(",");
                }
                updateBudgetCategories(csvList.toString(),parentName);
            } while (cursor.moveToNext());
        }
    }
    public void deleteBudget(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_NAME +" WHERE "+ COL1 +" = \""+ name +"\"";
        db.execSQL(query);
    }
    public void updateBudgetTotal(String total,String difference, String parentName){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_NAME +" SET "+ COL2 +" = \""+ total +"\" , "+COL3+"=\""+difference+"\" WHERE " + COL1 + " = \"" + parentName+"\"";
        db.execSQL(query);
    }
    public boolean addTransaction(String name, String parentName, String amount, String description){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TRANSACTIONCOL1, parentName);
        contentValues.put(TRANSACTIONCOL2, name);
        contentValues.put(TRANSACTIONCOL3, amount);
        contentValues.put(TRANSACTIONCOL4, description);
        Log.d(TAG, "addData: Adding " + name + " to "+ TRANSACTION_TABLE_NAME);
        long result = db.insert(TRANSACTION_TABLE_NAME, null, contentValues);
        if (result == -1) {
            return false;
        }
        else{
            return true;
        }

    }

    public Cursor getTransactions(String parentName, String name){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TRANSACTION_TABLE_NAME+" WHERE "+TRANSACTIONCOL1+"= \""+parentName+"\" AND "+ TRANSACTIONCOL2+"= \""+ name+"\"";
        Cursor data = db.rawQuery(query,null);
        return data;
    }
    public Cursor getTransaction(String transactionId){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TRANSACTION_TABLE_NAME+" WHERE "+TRANSACTIONCOL0+"= \""+transactionId+"\"";
        Cursor data = db.rawQuery(query,null);
        return data;
    }
    public void updateTransaction(String description, String amount, String id){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TRANSACTION_TABLE_NAME +" SET "+ TRANSACTIONCOL4 +" = \""+ description +"\"," + TRANSACTIONCOL3 + " = \"" + amount+"\" WHERE "+TRANSACTIONCOL0+"=\""+id+"\"";
        db.execSQL(query);
    }
    public void deleteTransaction(String id){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TRANSACTION_TABLE_NAME +" WHERE "+ TRANSACTIONCOL0 +" = \""+ id +"\"";
        db.execSQL(query);
    }
}
