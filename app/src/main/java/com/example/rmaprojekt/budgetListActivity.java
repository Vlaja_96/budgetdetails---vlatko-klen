package com.example.rmaprojekt;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class budgetListActivity extends AppCompatActivity {
    private Button createButton;

    private static final String TAG = "ListDataActivity";
    DatabaseHelper mDatabaseHelper;
    private ListView mListView;
    private List<String> nameList, IDList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget_list);
        nameList = new ArrayList<String>();
        IDList = new ArrayList<String>();
        mDatabaseHelper = new DatabaseHelper(this);
        mListView = (ListView) findViewById(R.id.budgetList);
        createButton = (Button) findViewById(R.id.budgetCreate);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCreateBudget();
            }
        });
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String currentBudget = nameList.get(i);
                String budgetID = IDList.get(i);
                openBudget(currentBudget);
            }
        });
        populateListView();

    }

    private void populateListView() {
        Log.d(TAG, "populateListView: Displaying data in the budget list.");
        Cursor data = mDatabaseHelper.getData();

        ArrayList<String> listData = new ArrayList<>();
        if (data.moveToFirst()) {
            do {
                String ID = data.getString(data.getColumnIndex("ID"));
                String name = data.getString(data.getColumnIndex("name"));
                String budgetDifference = data.getString(data.getColumnIndex("budgetDifference"));
                listData.add(name + ", Balance: "+budgetDifference+" kn");
                nameList.add(name);
                IDList.add(ID);
            } while (data.moveToNext());
        }
        ListAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listData);
        mListView.setAdapter(adapter);
    }

    public void openCreateBudget(){
        Intent intent = new Intent(this, budgetCreateActivity.class);
        startActivity(intent);
    }
    public void openBudget(String name){
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("currentBudget", name);
        startActivity(intent);
    }
}
