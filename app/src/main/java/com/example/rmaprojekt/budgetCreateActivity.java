package com.example.rmaprojekt;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class budgetCreateActivity extends AppCompatActivity {

    private EditText budgetName;
    private Button createBudgetButton;
    DatabaseHelper mDatabaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget_create);

        budgetName = (EditText) findViewById(R.id.createBudgetName);
        createBudgetButton = (Button) findViewById(R.id.createBudgetButton);
        mDatabaseHelper = new DatabaseHelper(this);
        createBudgetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newEntry = budgetName.getText().toString();

                if(budgetName.length() != 0){
                    AddData(newEntry);
                }
                else{
                    toastMessage("You cannot leave name empty");
                }
            }
        });
    }
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, budgetListActivity.class);
        startActivity(intent);
    }

    public void AddData(String newEntry){
        boolean insertData = mDatabaseHelper.addData(newEntry);

        if(insertData){
            toastMessage("Data Successfully Inserted!");
            onSuccessfulCreate();
        }
        else{
            toastMessage("Something went wrong.");
        }
    }
    private void onSuccessfulCreate(){
        Intent intent = new Intent(this, budgetListActivity.class);
        startActivity(intent);
    }
    private void toastMessage(String message){
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
    }
}
