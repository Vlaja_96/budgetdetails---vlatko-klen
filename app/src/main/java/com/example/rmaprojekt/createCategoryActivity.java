package com.example.rmaprojekt;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class createCategoryActivity extends AppCompatActivity {

    private String parentName;
    private EditText categoryName;
    private Button createButton;
    DatabaseHelper mDatabaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_category);
        parentName = getIntent().getExtras().get("parentName").toString();
        categoryName = (EditText) findViewById(R.id.createCategoryName);
        createButton = (Button) findViewById(R.id.createCategoryButton);
        mDatabaseHelper = new DatabaseHelper(this);

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newEntry = categoryName.getText().toString();

                if(categoryName.length() != 0){
                    AddData(newEntry,parentName);
                    categoryName.setText("");
                }
                else{
                    toastMessage("You cannot leave name empty");
                }
            }
        });
    }

    private void toastMessage(String message) {
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
    }

    private void AddData(String name, String parentName) {
        boolean insertData = mDatabaseHelper.addCategory(name, parentName);

        if(insertData){
            toastMessage("Data Successfully Inserted!");
            onSuccessfulCreate();
        }
        else{
            toastMessage("Something went wrong.");
        }
    }
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("currentBudget", parentName);
        startActivity(intent);
    }
    private void onSuccessfulCreate(){
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("currentBudget", parentName);
        startActivity(intent);
    }
}