package com.example.rmaprojekt;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class createTransactionActivity extends AppCompatActivity {

    private String parentName , name;
    private EditText transactionDescription, transactionAmount;
    private Button createButton;
    DatabaseHelper mDatabaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_transaction);

        Intent intent = getIntent();
        if (intent.hasExtra("parentName")) {
            parentName = getIntent().getExtras().get("parentName").toString();
        } else {
            parentName = "";
        }
        if (intent.hasExtra("name")) {
            name = getIntent().getExtras().get("name").toString();
        } else {
            name = "";
        }
        transactionDescription = (EditText) findViewById(R.id.createTransactionDescription);
        transactionAmount = (EditText) findViewById(R.id.createTransactionAmount);
        createButton = (Button) findViewById(R.id.transactionCreateButton);
        mDatabaseHelper = new DatabaseHelper(this);

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String description = transactionDescription.getText().toString();
                String amount = transactionAmount.getText().toString();

                if(transactionDescription.length() != 0){
                    AddData(description, amount,name,parentName);
                    transactionDescription.setText("");
                }
                else{
                    toastMessage("You cannot leave description empty");
                }
            }
        });
    }

    private void toastMessage(String message) {
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
    }

    private void AddData(String description, String amount, String name, String parentName) {
        boolean insertData = mDatabaseHelper.addTransaction(name, parentName, amount, description);

        if(insertData){
            toastMessage("Data Successfully Inserted!");
            onSuccessfulCreate();
        }
        else{
            toastMessage("Something went wrong.");
        }
    }
    private void onSuccessfulCreate(){
        Intent intent = new Intent(this, budgetCategoryActivity.class);
        intent.putExtra("parentName", parentName);
        intent.putExtra("name", name);
        startActivity(intent);
    }
    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this, budgetCategoryActivity.class);
        intent.putExtra("parentName", parentName);
        intent.putExtra("name", name);
        startActivity(intent);
    }
}