package com.example.rmaprojekt;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public  Integer totalSpent;
    public  String currentBudget, totalBudget, categories, spentBudget;
    DatabaseHelper mDatabaseHelper;
    public  EditText budgetTotalAmount;
    public  TextView budgetSpentAmount, budgetDifferenceAmount, budgetTitle;
    public  ListView mListView;
    public  List<String> categoryList;
    public  Button createCategory, deleteBudgetBtn;
    public  static final String TAG = "ListDataActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        categoryList = new ArrayList<String>();
        totalSpent=0;

        Intent intent = getIntent();

        if (intent.hasExtra("currentBudget")) {
            currentBudget = getIntent().getExtras().get("currentBudget").toString();
        } else {
            currentBudget = "";
        }
        mDatabaseHelper = new DatabaseHelper(this);
        budgetTitle = (TextView) findViewById(R.id.budgetTitle);
        createCategory = (Button) findViewById(R.id.createCategoryButton);
        budgetTotalAmount = (EditText) findViewById(R.id.budgetTotalAmount);
        budgetSpentAmount = (TextView) findViewById(R.id.budgetSpentAmount);
        budgetDifferenceAmount = (TextView) findViewById(R.id.budgetDifferenceAmount);
        mListView = (ListView) findViewById(R.id.categoriesListView);
        deleteBudgetBtn = (Button) findViewById(R.id.deleteBudget);
        deleteBudgetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteBudget(currentBudget);
            }
        });
        createCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openCreateCategory();
            }
        });
        budgetTotalAmount.addTextChangedListener((new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if(budgetTotalAmount.length()>0) {
                    Integer difference = getDifference();
                    budgetDifferenceAmount.setText(difference + " kn");
                    mDatabaseHelper.updateBudgetTotal(budgetTotalAmount.getText().toString().replaceAll("[^\\d]", ""),difference.toString(), currentBudget);
                }
            }
        }));
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String currentCategory = categoryList.get(i);
                openCategory(currentCategory);
            }
        });
        populateBudget();
        populateListView();
        budgetTitle.setText(currentBudget);
        budgetSpentAmount.setText(totalSpent.toString()+" kn");
        budgetDifferenceAmount.setText(getDifference()+" kn");
        mDatabaseHelper.updateBudgetTotal(budgetTotalAmount.getText().toString().replaceAll("[^\\d]", ""),getDifference().toString(), currentBudget);
    }

    public  void deleteBudget(String budget) {
        mDatabaseHelper.deleteBudget(budget);
        Intent intent = new Intent(this, budgetListActivity.class);
        startActivity(intent);
    }

    public Integer getDifference() {
        if(currentBudget.isEmpty()) return 0;
        int total = Integer.parseInt(budgetTotalAmount.getText().toString().replaceAll( "[^\\d]", "" ));
        int spent = Integer.parseInt(budgetSpentAmount.getText().toString().replaceAll( "[^\\d]", "" ));
        int difference = total - spent;
        return difference;
    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(this,budgetListActivity.class);
        startActivity(intent);
    }

    public void openCategory(String currentCategory){
        Intent intent = new Intent(this, budgetCategoryActivity.class);
        intent.putExtra("parentName", currentBudget);
        intent.putExtra("name", currentCategory);
        startActivity(intent);
    }

    public  void populateBudget() {
        Cursor cursor = mDatabaseHelper.getBudget(currentBudget);
        if (cursor.moveToFirst()) {
            do {
                totalBudget = cursor.getString(cursor.getColumnIndex("totalBudget"));
                spentBudget = cursor.getString(cursor.getColumnIndex("budgetDifference"));
                categories = cursor.getString(cursor.getColumnIndex("categories"));
                budgetTotalAmount.setText(totalBudget);
            } while (cursor.moveToNext());
        }
    }
    public void openCreateCategory(){
        Intent intent = new Intent(this, createCategoryActivity.class);
        intent.putExtra("parentName", currentBudget);
        startActivity(intent);
    }
    public  void populateListView() {
        if(currentBudget.isEmpty()) return;
        String[] items = categories.split(",");
        List<String> list = new ArrayList<String>();
        for(int i=0; i < items.length; i++){
            if(items[i].length()>0){
                String sum = getTransactionsSum(items[i]);
                categoryList.add(items[i]);
                list.add(items[i]+": "+sum+" kn");
            }
        }
        ListAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
        mListView.setAdapter(adapter);
    }

    public  String getTransactionsSum(String item) {
        Cursor cursor = mDatabaseHelper.getTransactions(currentBudget, item);
        Integer sum = 0;
        if (cursor.moveToFirst()) {
            do {
                String amount = cursor.getString(cursor.getColumnIndex("transactionAmount"));
                sum+= Integer.parseInt(amount);
            } while (cursor.moveToNext());
        }
        totalSpent+=sum;
        return sum.toString();
    }
}
