package com.example.rmaprojekt;

import android.app.Instrumentation;
import android.view.View;
import android.widget.Button;

import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertNotNull;

public class budgetListActivityTest {

    @Rule
    public ActivityTestRule<budgetListActivity> mActivityTestRule = new ActivityTestRule<budgetListActivity>(budgetListActivity.class);

    private budgetListActivity mActivity = null;

    @Before
    public void setUp() throws Exception {

        mActivity = mActivityTestRule.getActivity();
    }
    @After
    public void tearDown() throws Exception {

        mActivity = null;
    }


    @Test
    public void onLaunch() {

        View view = mActivity.findViewById(R.id.budgetCreate);

        assertNotNull(view);
    }

    @Test
    public void openCreateBudget() throws Throwable {
        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(budgetCreateActivity.class.getName(), null, false);
        final Button budgetCreate = (Button) mActivity.findViewById(R.id.budgetCreate);

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                budgetCreate.performClick();
            }
        });

        budgetCreateActivity nextActivity = (budgetCreateActivity) getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);

        assertNotNull("create budget activity not launched",nextActivity);
        nextActivity .finish();
    }

    @Test
    public void openBudget() throws Throwable {
        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(MainActivity.class.getName(), null, false);


        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                mActivity.openBudget("");
            }
        });

        MainActivity nextActivity = (MainActivity) getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);

        assertNotNull("budget activity not launched",nextActivity);
        nextActivity .finish();
    }
}