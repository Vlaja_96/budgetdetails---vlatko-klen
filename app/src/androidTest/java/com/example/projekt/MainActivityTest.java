package com.example.rmaprojekt;

import android.app.Instrumentation;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    private MainActivity mActivity = null;

    @Before
    public void setUp() throws Exception {

        mActivity = mActivityTestRule.getActivity();
    }
    @After
    public void tearDown() throws Exception {

        mActivity = null;
    }


    @Test
    public void onLaunch() {

        View view = mActivity.findViewById(R.id.createCategoryButton);

        assertNotNull(view);
    }
    @Test
    public void getDifferencePositiveValue() {
        mActivity.currentBudget="Placeholder";
        EditText budgetTotalAmount = (EditText) mActivity.findViewById(R.id.budgetTotalAmount);
        TextView budgetSpentAmount = (TextView) mActivity.findViewById(R.id.budgetSpentAmount);

        budgetTotalAmount.setText("1000");
        budgetSpentAmount.setText("200");
        int actual = mActivity.getDifference();

        assertEquals(800,actual);
    }
    @Test
    public void getDifferenceNegativeValue() {
        mActivity.currentBudget="Placeholder";
        EditText budgetTotalAmount = (EditText) mActivity.findViewById(R.id.budgetTotalAmount);
        TextView budgetSpentAmount = (TextView) mActivity.findViewById(R.id.budgetSpentAmount);

        budgetTotalAmount.setText("1000");
        budgetSpentAmount.setText("1300");
        int actual = mActivity.getDifference();

        assertEquals(-300,actual);
    }

    @Test
    public void openCreateCategory() throws Throwable {
        mActivity.currentBudget="Placeholder";
        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(createCategoryActivity.class.getName(), null, false);

        final Button createCategoryButton = (Button) mActivity.findViewById(R.id.createCategoryButton);

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                createCategoryButton.performClick();
            }
        });

        createCategoryActivity nextActivity = (createCategoryActivity) getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);

        assertNotNull("create category activity not launched",nextActivity);
        nextActivity .finish();

    }
}