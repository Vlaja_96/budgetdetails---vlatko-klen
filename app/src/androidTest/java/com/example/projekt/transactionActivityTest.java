package com.example.rmaprojekt;

import android.app.Instrumentation;
import android.view.View;
import android.widget.Button;

import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertNotNull;

public class transactionActivityTest {

    @Rule
    public ActivityTestRule<transactionActivity> mActivityTestRule = new ActivityTestRule<transactionActivity>(transactionActivity.class);

    private transactionActivity mActivity = null;

    @Before
    public void setUp() throws Exception {

        mActivity = mActivityTestRule.getActivity();
    }
    @After
    public void tearDown() throws Exception {

        mActivity = null;
    }


    @Test
    public void onLaunch() {

        View view = mActivity.findViewById(R.id.transactionSave);

        assertNotNull(view);
    }

    @Test
    public void saveTransaction() throws Throwable {

        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(budgetCategoryActivity.class.getName(), null, false);
        final Button transactionSave = (Button) mActivity.findViewById(R.id.transactionSave);

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                transactionSave.performClick();
            }
        });

        budgetCategoryActivity nextActivity = (budgetCategoryActivity) getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);

        assertNotNull("Transaction didn't save succesfully",nextActivity);
        nextActivity .finish();

    }

    @Test
    public void deleteTransaction() throws Throwable {


        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(budgetCategoryActivity.class.getName(), null, false);
        final Button transactionDelete = (Button) mActivity.findViewById(R.id.transactionDelete);

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                transactionDelete.performClick();
            }
        });

        budgetCategoryActivity nextActivity = (budgetCategoryActivity) getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);

        assertNotNull("Transaction didn't delete succesfully",nextActivity);
        nextActivity .finish();

    }
}