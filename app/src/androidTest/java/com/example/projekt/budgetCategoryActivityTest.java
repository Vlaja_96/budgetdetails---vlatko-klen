package com.example.rmaprojekt;

import android.app.Instrumentation;
import android.view.View;
import android.widget.Button;

import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertNotNull;

public class budgetCategoryActivityTest {


    @Rule
    public ActivityTestRule<budgetCategoryActivity> mActivityTestRule = new ActivityTestRule<budgetCategoryActivity>(budgetCategoryActivity.class);

    private budgetCategoryActivity mActivity = null;

    @Before
    public void setUp() throws Exception {

        mActivity = mActivityTestRule.getActivity();
    }
    @After
    public void tearDown() throws Exception {

        mActivity = null;
    }


    @Test
    public void onLaunch() {

        View view = mActivity.findViewById(R.id.transactionCreateButton);

        assertNotNull(view);
    }
    @Test
    public void openCreateTransaction() throws Throwable {
        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(createTransactionActivity.class.getName(), null, false);
        final Button transactionCreateButton = (Button) mActivity.findViewById(R.id.transactionCreateButton);

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                transactionCreateButton.performClick();
            }
        });

        createTransactionActivity nextActivity = (createTransactionActivity) getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);

        assertNotNull("create budget activity not launched",nextActivity);
        nextActivity .finish();
    }

    @Test
    public void deleteCategory() throws Throwable {

        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(MainActivity.class.getName(), null, false);

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                mActivity.deleteCategory();
            }
        });

        MainActivity nextActivity = (MainActivity) getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);

        assertNotNull("Category not deleted successfuly",nextActivity);
        nextActivity .finish();
    }
}