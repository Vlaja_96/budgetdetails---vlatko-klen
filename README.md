﻿
Testovi su ovdje: https://gitlab.com/Vlaja_96/budgetdetails---vlatko-klen/tree/master/app/src/androidTest/java/com/example/projekt

BudgetDetails je aplikacija za Android mobilne uređaje koja služi za jednostavno stvaranje te kontroliranje budžeta, omogućuje unos i pregled transakcija te kategoriziranje.
Aplikacija na prvom zaslonu prikazuje listu svih budžeta, njihovih stanja, te dugme za kreiranje. 
Prilikom kreiranja korisniku se na novom zaslonu ponudi stvaranje novog buđeta te postavljanje imena.
Odabirom nekog od budžeta na novom zaslonu se prikazuju njegovi detalji te lista kategorija kao njihovo stanje. 
Može se vidjeti i urediti sveukupni budžet, vidi se količina novaca potrošena, te stanje kao razlika sveukupnog i potrošenog.
Na prikazu budžeta može se dodati nova kategorija te se vidi lista kategorija i sveukupno novaca potrošeno u svakoj. 
Klikom na kategoriju otvori se novi prikaz koji sadrži dugme za kreiranje transakcija, te listu transakcija.
Svaka transakcija sastoji se od opisa transakcije te iznosa, zbroj svih transakcija predstavlja količinu novaca potrošenu u kategoriji.
Klikom na gumb za kreiranje transakcije novi prozor se otvori koji omogućava korisniku unos veličine transakcije te njen opis.
Klikom na neku od transakcija u listi otvara se prikaz koji omogućuje izmjenjivanje podataka transakcije te njeno brisanje. 
Svi ti podaci spremaju se u lokalnu bazu podataka.

